package com.edu.qinjuan.demoactivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class CareerActivity extends Activity 
{

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_career);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.career, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) 
		{
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	public void onBtn4Click(View view) 
	{
		Intent intent = getIntent();
		intent.putExtra("career", "公务员");
		setResult(RESULT_OK, intent);
		finish();
		
	}

	public void onBtn5Click(View view) 
	{
		Intent intent = getIntent();
		intent.putExtra("career", "医生");
		setResult(RESULT_OK, intent);
		finish();
	}

	public void onBtn6Click(View view) 
	{
		Intent intent = getIntent();
		intent.putExtra("career", "教师");
		setResult(RESULT_OK, intent);
		finish();
	}
}
