package com.edu.qinjuan.demoactivity;

import android.widget.*;
import  com.edu.qinjuan.demoactivity.R;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends Activity 
{
	public static final int REQUEST_CITY = 0x01; 
	public static final int REQUEST_CAREER = 0x02; 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
    
    public void onBtnCityClick(View view) {
		Intent intent = new Intent(MainActivity.this, CityActivity.class);
		startActivityForResult(intent, this.REQUEST_CITY); 
		}
	public void onBtnCareerClick(View view) {
		Intent intent = new Intent(MainActivity.this, CareerActivity.class);
		startActivityForResult(intent, this.REQUEST_CAREER);
		}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		switch (requestCode) {
		case REQUEST_CITY: 
		if (resultCode == RESULT_OK) {
		EditText etCity = (EditText) findViewById(R.id.et_city);
		etCity.setText(data.getStringExtra("city"));
		}
		
		break;
		case REQUEST_CAREER:
		if (resultCode == RESULT_OK) {
		EditText etCareer = (EditText) findViewById(R.id.et_career);
		etCareer.setText(data.getStringExtra("career"));
		}
		break;
		}
	}

}
